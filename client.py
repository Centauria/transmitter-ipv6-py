# -*- coding: utf-8 -*-
import os
import time
import zlib
import argparse
import ipaddress
from typing import Union, List
from tqdm import tqdm

from util import *


class Client:
    def __init__(self, host: Union[ipaddress.IPv6Address, str] = '::1', port=DEFAULT_PORT,
                 files: Optional[List] = None, without_crc32=False):
        if files is None:
            files = []
        if isinstance(host, ipaddress.IPv6Address):
            self.host = host.compressed
        else:
            self.host = host
        self.port = port
        self.socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self.files = files
        self.without_crc32 = without_crc32
        self.closed_files = []
        self.socket.connect((self.host, self.port))
        self.frame_id = 0
        self.current_file_frame_id = None
        self.max_rate = None

    def send_text(self, text: str):
        self.socket.send(Protocol.encode_text_frame(self.frame_id, text))

    def send(self, file_name):
        assert file_name in self.files
        size = os.path.getsize(file_name)
        opened_file = open(file_name, 'rb')
        if self.without_crc32:
            crc32 = 0
        else:
            crc32 = zlib.crc32(opened_file.read())
        self.socket.send(Protocol.encode_information_frame(self.frame_id, size, crc32, file_name))
        response = self.socket.recv(BUFF_SIZE)
        if len(response) == 0:
            log.error('Connection terminated by server')
        response = Protocol.decode(response)
        if response['id'] == self.frame_id:
            if response['respond'] == 'normal':
                opened_file.seek(0)
                if 'add' in response.keys():
                    self.max_rate = response['add']
                    print('Max rate = {} B/s'.format(self.max_rate))
                else:
                    self.max_rate = None
                    print('Max rate = INF')
            elif response['respond'] == 'stop' or response['respond'] == 'pause':
                print('Server has cancelled the transmission')
                return
        self.frame_id += 1
        if self.max_rate is None:
            delta = 0
            data_size = MAX_DATA_SIZE
        else:
            delta = 1 / MIN_UPDATE_RATE
            data_size = self.max_rate * delta
            if data_size < MIN_DATA_SIZE:
                delta = MIN_DATA_SIZE / self.max_rate
                data_size = MIN_DATA_SIZE
            elif data_size > MAX_DATA_SIZE:
                delta = MAX_DATA_SIZE / self.max_rate
                data_size = MAX_DATA_SIZE
            data_size = int(data_size)
        t = tqdm(total=size, unit='B', unit_scale=True)
        while True:
            position = opened_file.tell()
            data = opened_file.read(data_size)
            log.info('Read file: pos={}, size={}'.format(position, len(data)))
            if data == b'':
                data_frame = Protocol.encode_data_frame(self.frame_id, position, data, end=True)
                self.socket.send(data_frame)
                opened_file.close()
                break
            elif len(data) == data_size:
                data_frame = Protocol.encode_data_frame(self.frame_id, position, data)
                self.socket.send(data_frame)
            elif len(data) < data_size:
                data_frame = Protocol.encode_data_frame(self.frame_id, position, data, end=True)
                self.socket.send(data_frame)
            response = self.socket.recv(BUFF_SIZE)
            if len(response) == 0:
                log.error('Connection terminated by server')
                break
            response = Protocol.decode(response)
            if response['id'] == self.frame_id:
                if response['respond'] == 'normal':
                    t.update(len(data))
                elif response['respond'] == 'stop':
                    print('Server has cancelled the transmission')
                elif response['respond'] == 'pause':
                    print('Server has paused the transmission')
            if delta > 0:
                time.sleep(delta)
            self.frame_id += 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Transmit files to others.')

    # nargs='*' means nargs>=0, nargs='+' means nargs>=1
    parser.add_argument('files', metavar='File', type=str, nargs='*',
                        help='files to be transferred')
    parser.add_argument('-d', '--dest', dest='destination', type=str,
                        help='the destination of the transmitter')
    parser.add_argument('-p', '--port', dest='destination_port', type=int, default=DEFAULT_PORT,
                        help='Destination port')
    parser.add_argument('--without-crc32', action='store_true',
                        help='Do not calculate CRC32 of files in advance, it works for big files')

    args = parser.parse_args()
    client = Client(args.destination, args.destination_port, args.files, args.without_crc32)
    for f in client.files:
        client.send(f)
