# -*- coding: utf-8 -*-
import socket
import logging
import struct
import base64
from typing import Optional

# logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger()

DEFAULT_PORT = 23456
BUFF_SIZE = 1 << 16
MAX_DATA_SIZE = 48000
MIN_DATA_SIZE = 1024
MIN_UPDATE_RATE = 10


def get_host_ipv6():
    s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
    try:
        s.connect(('centauriavc.com', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()

    return ip


class ProtocolDecodeError(ValueError):
    pass


class Protocol:
    @staticmethod
    def analyze(data: bytes):
        flags = data[0]
        if bool(flags & 0b11 == 0b11):
            return 'response'
        if bool(flags & 0b01000000):
            return 'info'
        if flags >> 7 == 1:
            return 'data'
        elif flags >> 7 == 0:
            return 'text'
        raise ProtocolDecodeError

    @staticmethod
    def decode(data: bytes):
        result = dict(type=Protocol.analyze(data))
        flags = data[0]
        data = base64.decodebytes(data[1:-1])
        if result['type'] == 'response':
            feed_back = (flags & 0b1100) >> 2
            if feed_back == 0:
                result['respond'] = 'normal'
            elif feed_back == 1:
                result['respond'] = 'pause'
            elif feed_back == 3:
                result['respond'] = 'stop'
            else:
                raise ProtocolDecodeError
            result['id'], = struct.unpack('!I', data[:4])
            if bool(flags & 0b00010000):
                result['add'], = struct.unpack('!I', data[4:])
            return result
        if result['type'] == 'info':
            result['id'], result['size'], result['crc32'] = struct.unpack('!3I', data[:12])
            result['name'] = data[12:].decode()
            return result
        if result['type'] == 'data':
            result['id'], result['pos'] = struct.unpack('!2I', data[:8])
            result['data'] = data[8:]
            result['end'] = flags & 1
            return result
        if result['type'] == 'text':
            result['id'], = struct.unpack('!I', data[:4])
            result['text'] = data[4:].decode()
            return result

    @staticmethod
    def encode_text_frame(frame_id: int, text: str) -> bytes:
        flags = (0b00000001).to_bytes(1, byteorder='big')
        frame_id = frame_id.to_bytes(4, byteorder='big')
        result = flags + base64.b64encode(frame_id + text.encode()) + b'\n'
        return result

    @staticmethod
    def encode_data_frame(frame_id: int, pos: int, data: bytes, end: bool = False) -> bytes:
        flags = (0b10000000 | end).to_bytes(1, byteorder='big')
        frame_id = frame_id.to_bytes(4, byteorder='big')
        pos = pos.to_bytes(4, byteorder='big')
        result = flags + base64.b64encode(frame_id + pos + data) + b'\n'
        return result

    @staticmethod
    def encode_information_frame(frame_id: int, size: int, crc32: int, name: str) -> bytes:
        flags = (0b11000001).to_bytes(1, byteorder='big')
        frame_id = frame_id.to_bytes(4, byteorder='big')
        size = size.to_bytes(4, byteorder='big')
        crc32 = crc32.to_bytes(4, byteorder='big')
        result = flags + base64.b64encode(frame_id + size + crc32 + name.encode()) + b'\n'
        return result

    @staticmethod
    def encode_response_frame(frame_id: int, response_type: str = 'normal', add: Optional[int] = None) -> bytes:
        has_additional_info = int(add is not None)
        if response_type == 'normal':
            response = 0
        elif response_type == 'pause':
            response = 1
        elif response_type == 'stop':
            response = 3
        else:
            raise ValueError
        flags = (0b11000011 | has_additional_info << 4 | response << 2).to_bytes(1, byteorder='big')
        frame_id = frame_id.to_bytes(4, byteorder='big')
        if has_additional_info:
            encoded = frame_id + add.to_bytes(4, byteorder='big')
        else:
            encoded = frame_id
        result = flags + base64.b64encode(encoded) + b'\n'
        return result
