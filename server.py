# -*- coding: utf-8 -*-
import argparse
import ipaddress
import socketserver as ss
from tqdm import tqdm

from util import *


class Handler(ss.StreamRequestHandler):
    def setup(self):
        super().setup()
        log.info('New connection {}'.format(self.client_address))

    def handle(self):
        t = None
        current_file = None
        try:
            while True:
                data = self.rfile.readline()
                if data == b'':
                    log.info('Connection lost')
                    break
                recv = Protocol.decode(data)
                if recv['type'] == 'text':
                    log.info(recv['text'])
                elif recv['type'] == 'data':
                    if current_file is not None and not current_file.closed:
                        current_file.seek(recv['pos'])
                        current_file.write(recv['data'])
                        t.update(len(recv['data']))
                        if recv['end'] == 1:
                            print('File received successfully.')
                            current_file.close()
                        log.info('Received: id={}, pos={}, end={}'.format(recv['id'], recv['pos'], recv['end']))
                        self.respond(recv['id'], 'normal')
                elif recv['type'] == 'info':
                    print('Client {} wants to share a file \n'
                          'File name: {} \n'
                          'Size: {}\n'
                          'CRC32={}\n'
                          'Do you want to receive it? (y/N): '
                          .format(self.client_address[0], recv['name'], recv['size'], recv['crc32']), end='')
                    while True:
                        choose = input()
                        if choose == '' or choose == 'n' or choose == 'N':
                            self.respond(recv['id'], 'stop')
                            break
                        elif choose == 'y' or choose == 'Y':
                            print('How fast do you want? (in KB/s): ', end='')
                            rate = input()
                            try:
                                rate = int(float(rate) * 1024)
                            except ValueError:
                                print('No effective rate specified, run with no speed limit')
                                rate = None
                            print('Please give a file name (default is same as the name received): ', end='')
                            name = input()
                            if name == '':
                                name = recv['name']
                            current_file = open(name, 'wb')
                            self.respond(recv['id'], 'normal', rate)
                            t = tqdm(total=recv['size'], unit='B', unit_scale=True)
                            break
                        else:
                            print('Please choose y/n: ', end='')
        except ConnectionResetError:
            log.info('Connection reset')
        finally:
            if current_file is not None:
                current_file.close()

    def finish(self):
        super().finish()
        self.request.close()
        log.info('{} have exited'.format(self.client_address))

    def respond(self, frame_id, response_type, add: Optional[int] = None):
        self.wfile.write(Protocol.encode_response_frame(frame_id, response_type, add))


class IPv6Server(ss.ThreadingTCPServer):
    address_family = socket.AF_INET6


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Transmit files to others.')

    parser.add_argument('-p', '--port', dest='serve_port', type=int, default=DEFAULT_PORT,
                        help='Serve port')

    args = parser.parse_args()
    server = IPv6Server(('::', args.serve_port), Handler)
    host_ipv6 = ipaddress.IPv6Address(get_host_ipv6())
    print('Serving at {} port {}, Press Ctrl+D to terminate'.format(host_ipv6, args.serve_port))
    server.serve_forever()
