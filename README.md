# Transmitter-IPv6-py

Usage: 
```shell script
python server.py [-p SERVE_PORT]
python client.py [-d DESTINATION] [-p DESTINATION_PORT] [File [File ...]]
```
The server.py opens a ``ThreadingTCPServer`` to listen to given ``SERVE_PORT``.
The client.py opens a socket which have ``DESTINATION``, and it will try to connect to the destination through the ```DESTINATION_PORT```.

When the connection is established, if there's files to send, the server will ask you to receive it or ignore it.
If you choose receive it, then the server will ask you if you have a speed limit (default is no limit).
Then the server will ask you if you want to rename the file (default is no).

When the transmit proceeds, you can check the progress both on client and server.

## Protocol

We use a protocol for transferring different kind of data over the socket.

Notes:
- The frame sent by client must end with '\n' 
- The content behind the FLAGS is base64 encoded
- All the multi-byte integer use **big-endian**

The frame is divided into many kinds which we can see below.  

![Protocol instruction](img/protocol%20instruction.png)

All frames have their first byte as FLAGS and 2-5 bytes as ID.
ID is a 32 bit integer ascending during the client is transferring data.
e.g. the client send a frame of which ID is n, then the ID of next frame should be n+1.
In the response frame, ID is used to identify which frame the server response to.

The 3rd element of data frame is POS.
It contains where the data locates in the original file.
The start byte of the effective data is the POS-th byte in the original file.

The information frame is used to let server know the properties of the file.
- The 3rd element of the info frame is the size of the file (4 bytes).
- The 4th element of the info frame is the CRC32 of the file (4 bytes).
- The 5th element of the info frame is the name of the file.

The response frame is used by server to feedback.
- Before the transmission start, the server can use the response frame to choose the transmission rate.
In this case, the FLAGS[3] should be 1 to allow additional information.
Then the server respond a response frame with a 32-bit integer, which indicates the speed in bytes/s.
And the server can simply refuse the file by responding FLAGS[5]=1.