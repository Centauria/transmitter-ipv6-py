# Tasks

## ZhaoXue

1. ~~Write 2 functions in util.py, name specified. **Due in 11.14 14:00**~~
2. ~~Write a class ``FileHandler`` , which can load and save files~~
3. ~~Extend the server to handle multi messages~~
4. ~~Edit the class ``FileHandler`` to adapt to the protocol in README.~~
5. ~~Edit the class ``FileHandler`` to add new files to the ``FileHandler`` object.~~
6. ~~Edit the class ``FileHandler`` to add an ``exit`` method to safely close all files.~~
7. ~~Change the Protocol class to adapt to new protocol.~~

## Centauria

1. Others